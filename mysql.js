const mysql = require("mysql");

mySqlConnection = mysql.createPool({
    host: "localhost",
    user: "root",
    password: "fernando.123",
    database: "mydb",
    port: "3306"
});

module.exports = mySqlConnection;


/*
var mysql = require('mysql');

var pool = mysql.createPool({
    "user": "root",
    "password": "root",
    "database": "mydb",
    "host": "localhost",
    "port": 3306
});

module.exports = pool;
*/