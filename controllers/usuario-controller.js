const mysql = require('../mysql');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.insereUsuario = (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err) {
            return res.status(500).send({ error: err });
        } else {
            conn.query('select 1 from users where cnpj = ?', [req.body.cnpj], (err, results) => {
                if (err) {
                    return res.status(500).send({ error: err });
                } else if (results.length > 0) {
                    res.status(500).send({ error: "Usuario já cadastrado" });
                } else {
                    bcrypt.hash(req.body.password, 10, (errBcrypt, hash) => {
                        if (err) {
                            res.status(500).send({ error: errBcrypt });
                        } else {
                            conn.query(
                                `insert into users(fullname, cnpj, zipcode, city, state, country, email, password,address) 
                               value (?, ?, ?, ?,?, ?, ?, ?,?)`
                                ,[
                                    req.body.fullname,
                                    req.body.cnpj,
                                    req.body.zipcode,
                                    req.body.city,
                                    req.body.state,
                                    req.body.country,
                                    req.body.email,
                                    hash,
                                    req.body.address
                                ],
                                (error, result, fields) => {
                                    conn.release();
                                    if (error) {
                                        res.status(500).send({
                                            error: error
                                        });
                                    } else {
                                        let token = jwt.sign({
                                            fullname: req.body.fullname,
                                            cnpj: req.body.cnpj,
                                            password: req.body.password,
                                            zipcode: req.body.zipcode,
                                            city: req.body.city,
                                            state: req.body.state,
                                            country: req.body.country,
                                            email: req.body.email,
                                            address: req.body.address,
                                        }, "querino", {});
                                        res.status(201).send({
                                            message: "Sucesso",
                                            id_usuario: result.insertId,
                                            token: token
                                        });
                                    }
                                }
                            );
                        }
                    });
                }
            });
        }
    });
}

exports.login = (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err) { return res.status(500).send({ error: err }) }
        conn.query(`select * from users where cnpj = ?`, [req.body.cnpj], (error, results) => {
            conn.release();
            if (error) { return res.status(500).send({ error: error }) }
            if (results.length < 1) { return res.status(401).send({ error: "Usuario não existe" }) }
            else {
                bcrypt.compare(req.body.password, results[0].password, (errBcrypt, result) => {
                    if (errBcrypt) { return res.status(401).send({ error: "Erro senha" }) }
                    if (result) {
                        let token = jwt.sign({
                            cnpj: results[0].cnpj,
                            fullname: results[0].fullname,
                            id: results[0].id
                        }, "querino", {});
                        return res.status(200).send({
                            messagem: "Sucesso",
                            cnpj: results[0].cnpj,
                            fullname: results[0].fullname,
                            id_usuario: results[0].id_usuario,
                            token: token
                        })
                    } else {
                        return res.status(401).send({ error: "Falha na autenticação" })
                    }
                });
            }
        });
    });
}

exports.getUsuarios = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            'select * from users', (error, result, fields) => {
                conn.release();
                if (error) { res.status(500).send({ error: error }) }
                else { res.status(200).send({ message: "Sucesso", users: result }) }
            }
        );
    });
}