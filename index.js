const express = require("express");
const cors = require('cors');
const app = express();
const routerUsuarios = require("./routes/registerR");

let port = process.env.PORT || 3000;
let host = "localhost" 


app.use(express.urlencoded({ extended: false }));
app.use(express.json());


app.use(cors());


app.use('/user', routerUsuarios);


app.listen(port, host, () => {
    console.log("Listenning")
})

app.use((req, res, next) => {
    const erro = new Error('Não encontrado');
    erro.status = 404;
    next(erro);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        mensagem: error.message
    });
});

module.exports = app;
/*

const express = require('express');
const app = express();
const morgan = require('morgan');
var cors = require('cors');

const routeLancamentos = require('./routes/lancamentos-routes.js');
const routePurchases = require('./routes/purchases-routes');
const routeUsers = require('./routes/users-routes');
const routeProducts = require('./routes/products-routes');
const routeCart = require('./routes/cart-routes');


app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors());

app.use('/lancamentos', routeLancamentos);

app.use('/purchases', routePurchases);

app.use('/users', routeUsers);

app.use('/products', routeProducts);

app.use('/cart', routeCart);

app.use((req, res, next) => {
    const erro = new Error('Não encontrado');
    erro.status = 404;
    next(erro);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        mensagem: error.message
    });
});

module.exports = app;

*/